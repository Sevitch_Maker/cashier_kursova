create table role
(
    id        int         not null,
    role_name varchar(45) not null,
    primary key (id)
);

create table user
(
    id        int                not null auto_increment,
    name      varchar(45)        not null,
    surname   varchar(45)        not null,
    user_name varchar(45) unique not null,
    email     varchar(45) unique not null,
    password  varchar(100)       not null,
    role_id   int default 0      not null,
    active    boolean            not null,
    primary key (id),
    constraint role_id
        foreign key (role_id)
            references role (id)
);

create INDEX role_id_idx on user (role_id ASC);

create table product
(
    id     int         not null,
    name   varchar(70) not null,
    count  int         not null,
    weight double      not null,
    price  decimal     not null,
    primary key (id)
);

create table check_list
(
    id             int         not null auto_increment,
    product_id     int         not null,
    product_name   varchar(70) not null,
    product_price  decimal     not null,
    product_weight double      not null,
    product_count  int         not null,
    cashier_id     int         not null,
    primary key (id),
    constraint cashier_id
        foreign key (cashier_id)
            references user (id),
    constraint product_id
        foreign key (product_id)
            references product (id)
);

create INDEX cashier_id_idx on check_list (cashier_id ASC);