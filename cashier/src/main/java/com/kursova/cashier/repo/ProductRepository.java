package com.kursova.cashier.repo;

import com.kursova.cashier.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductRepository extends JpaRepository<Product, Long>{
    Product findById(Integer id);

    Product findByName(String name);
}