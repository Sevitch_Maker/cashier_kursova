package com.kursova.cashier.repo;

import com.kursova.cashier.entity.CheckList;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CheckListRepository extends JpaRepository<CheckList, Long>{
    CheckList findById(Integer id);
}