package com.kursova.cashier.repo;

import com.kursova.cashier.entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role, Long>{
    Role findById(Integer id);
}