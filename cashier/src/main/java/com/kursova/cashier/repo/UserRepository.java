package com.kursova.cashier.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.kursova.cashier.entity.User;

public interface UserRepository extends JpaRepository<User, Long> {
    User findByUserName(String userName);

    User findByEmail(String email);
}
