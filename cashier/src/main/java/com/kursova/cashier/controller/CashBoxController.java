package com.kursova.cashier.controller;

import com.kursova.cashier.repo.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class CashBoxController {

   @Autowired
   private ProductRepository productRepository;

   @GetMapping("/cashbox")
   public String index(Model model) {
      model.addAttribute("cashboxActive", true);
      model.addAttribute("checkList", productRepository.findAll());
      return "cashbox";
   }
}
