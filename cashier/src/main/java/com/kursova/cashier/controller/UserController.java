package com.kursova.cashier.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;

import com.kursova.cashier.repo.UserRepository;

@Controller
public class UserController {
    @Autowired
    private UserRepository userRepository;

    @GetMapping("/users")
    public String index(Model model) {
        model.addAttribute("usersActive", true);
        model.addAttribute("users", userRepository.findAll());
        return "users";
    }
}
