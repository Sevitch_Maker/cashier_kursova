package com.kursova.cashier.controller;

import com.kursova.cashier.entity.Product;
import com.kursova.cashier.repo.ProductRepository;
import com.kursova.cashier.service.auth.ProductService;
import com.kursova.cashier.validator.ProductValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class StockController {

   @Autowired
   private ProductService productService;

   @Autowired
   private ProductValidator productValidator;

   @Autowired
   private ProductRepository productRepository;

   @GetMapping("/stock")
   public String index(Model model) {
      model.addAttribute("stockActive", true);
      model.addAttribute("productForm", new Product());
      model.addAttribute("products", productRepository.findAll());
      return "stock";
   }

   @PostMapping("/stock/product")
   public String addProduct(@ModelAttribute("productForm") Product productForm, BindingResult bindingResult, Model model) {

      productValidator.validate(productForm, bindingResult);

      if (bindingResult.hasErrors()) {
         model.addAttribute("products", productRepository.findAll());
         return "stock";
      }

      productService.save(productForm);

      return "redirect:/stock";
   }

   @PostMapping("/stock/findproduct")
   public String search(@RequestParam(value = "search", required = false) String q, Model model) {

      model.addAttribute("products", productRepository.findByName(q));
      return "search-products";
   }
}
