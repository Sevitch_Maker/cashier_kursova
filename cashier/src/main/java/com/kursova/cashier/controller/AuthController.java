package com.kursova.cashier.controller;

import com.kursova.cashier.entity.User;
import com.kursova.cashier.repo.RoleRepository;;
import com.kursova.cashier.service.auth.SecurityService;
import com.kursova.cashier.service.auth.UserService;
import com.kursova.cashier.validator.UserValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.PermitAll;

@Controller
@PermitAll
@Validated
public class AuthController {

    @Autowired
    private UserService userService;

    @Autowired
    private RoleRepository roleRepo;

    @Autowired
    private SecurityService securityService;

    @Autowired
    private UserValidator userValidator;

    @GetMapping("/login")
    public String login() {
        return "auth/login";
    }

    @GetMapping("/registration")
    public String registration(Model model) {
        model.addAttribute("userForm", new User());
        model.addAttribute("userRoles", roleRepo.findAll());
        return "auth/registration";
    }

    @PostMapping("/register")
    public String register(@ModelAttribute("userForm") User userForm, BindingResult bindingResult, Model model) {

        userValidator.validate(userForm, bindingResult);

        if (bindingResult.hasErrors()) {
            model.addAttribute("userRoles", roleRepo.findAll());
            return "auth/registration";
        }
        userService.save(userForm);
        securityService.autoLogin(userForm.getUserName(), userForm.getPasswordConfirm());

        return "redirect:/";
    }
}
