package com.kursova.cashier.service.auth;

import com.kursova.cashier.entity.Product;

public interface ProductService {

    void save(Product product);
}