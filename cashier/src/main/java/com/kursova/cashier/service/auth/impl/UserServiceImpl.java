package com.kursova.cashier.service.auth.impl;

import com.kursova.cashier.entity.User;
import com.kursova.cashier.repo.UserRepository;
import com.kursova.cashier.service.auth.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;


@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    public void save(User user) {
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        user.setUserName(user.getEmail());
        user.setRoleId(user.getRoleId());
        user.setActive(true);
        userRepository.save(user);
    }
}
