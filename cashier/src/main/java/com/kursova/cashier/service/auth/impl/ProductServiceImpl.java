package com.kursova.cashier.service.auth.impl;

import com.kursova.cashier.entity.Product;
import com.kursova.cashier.repo.ProductRepository;
import com.kursova.cashier.service.auth.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    private ProductRepository productRepository;

    @Override
    public void save(Product product) {
        productRepository.save(product);
    }
}
