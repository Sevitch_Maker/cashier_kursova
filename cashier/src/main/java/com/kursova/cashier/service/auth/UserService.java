package com.kursova.cashier.service.auth;

import com.kursova.cashier.entity.User;

public interface UserService {

    void save(User user);
}