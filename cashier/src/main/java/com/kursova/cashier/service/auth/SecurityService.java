package com.kursova.cashier.service.auth;

public interface SecurityService {
    String findLoggedInUsername();

    void autoLogin(String email, String password);
}