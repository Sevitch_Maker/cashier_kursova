package com.kursova.cashier.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Entity
@Getter
@Setter
@Table(name = "product")
public class Product {

    @Id
    @NotNull
    private Integer id;

    @NotNull
    private String name;

    @NotNull
    private Integer count;

    @NotNull
    private double weight;

    @NotNull
    private BigDecimal price;
}
