package com.kursova.cashier.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Getter
@Setter
@Table(name = "role")
public class Role {

    @Id
    @NotNull
    private Integer id;

    @NotNull
    private String roleName;

}