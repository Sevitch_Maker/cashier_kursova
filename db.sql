create schema `kursova_cashier` default character set utf8;

use `kursova_cashier`;

create table `role`(
	`id` int not null,
    `role_name` varchar(45) not null,
    primary key (`id`)
);

create table `user`(
	`id` int not null auto_increment,
    `name` varchar(45) not null,
    `surname` varchar(45) not null,
    `email` varchar(45)  unique not null,
	`password` varchar(100) not null,
	`role_id` int default 0,
    primary key (`id`),
    index `role_id_idx` (`role_id` ASC),
    constraint `role_id`
		foreign key (`role_id`)
			references `role` (`id`)
);

create table `product`(
	`id` int not null,
    `name` varchar(70) not null,
    `count` int not null,
    `weight` double not null,
    `price` decimal not null,
    primary key (`id`)
);

create table `check`(
	`id` int not null auto_increment,
    `product_id` int not null,
    `product_name` varchar(70) not null,
    `product_price` decimal not null,
    `product_weight`  double not null,
	`product_count` int not null,
	`cashier_id` int not null,
    primary key (`id`),
    index `cashier_id_idx` (`cashier_id` ASC),
    constraint `cashier_id`
		foreign key (`cashier_id`)
			references `user` (`id`),
	constraint `product_id`
		foreign key (`product_id`)
			references `product` (`id`)
);

ALTER TABLE user
ADD active boolean;

